# manga-guide-to-cryptography

Rust cipher implementations from [The Manga Guide to Cryptography][1].

## TOC

- [Caesar cipher](src/bin/caesar.rs)

[1]: https://www.goodreads.com/book/show/34773223-the-manga-guide-to-cryptography