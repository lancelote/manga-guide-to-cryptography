fn modulo(x: i8, y: i8) -> i8 {
    (x % y + y) % y
}

pub fn encipher(plain_text: &str, key: i8) -> String {
    plain_text
        .chars()
        .map(|x| match x {
            x if x.is_alphabetic() => {
                let shift = match x {
                    x if x.is_lowercase() => 97,
                    x if x.is_uppercase() => 65,
                    _ => unreachable!(),
                };
                let code = x as u8 as i8 - shift + key;
                (modulo(code, 26) + shift) as u8 as char
            }
            _ => x,
        })
        .collect()
}

pub fn decipher(cipher_text: &str, key: i8) -> String {
    encipher(cipher_text, -key)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn encipher_momotaro() {
        assert_eq!(encipher("MOMOTARO", 3), String::from("PRPRWDUR"));
    }

    #[test]
    fn encipher_capital_z() {
        assert_eq!(encipher("Z", 3), String::from("C"));
    }

    #[test]
    fn encipher_capital_x() {
        assert_eq!(encipher("X", 3), String::from("A"));
    }

    #[test]
    fn negative_key_capital_a() {
        assert_eq!(encipher("A", -1), String::from("Z"));
    }

    #[test]
    fn test_decipher() {
        assert_eq!(decipher("PRPRWDUR", 3), String::from("MOMOTARO"));
    }

    #[test]
    fn non_alphabetic() {
        assert_eq!(encipher("MO, TO!", 3), String::from("PR, WR!"));
    }

    #[test]
    fn lowercase() {
        assert_eq!(encipher("momotaro", 3), String::from("prprwdur"));
    }

    #[test]
    fn mix_lower_and_upper_case() {
        assert_eq!(encipher("mOmOtArO", 3), String::from("pRpRwDuR"));
    }
}
