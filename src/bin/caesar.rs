extern crate manga_guide_to_cryptography;

use manga_guide_to_cryptography::cipher::caesar::{decipher, encipher};
use std::io;

fn main() {
    println!("encipher (e) or decipher (d)?");

    let mut mode = String::new();
    io::stdin().read_line(&mut mode).expect("can't read STDIN");

    println!("enter text: ");

    let mut text = String::new();
    io::stdin().read_line(&mut text).expect("can't read STDIN");
    text = String::from(text.trim());

    println!("enter the key: ");

    let mut key = String::new();
    io::stdin().read_line(&mut key).expect("can't read STDIN");
    let key: i8 = key.trim().parse().expect("can't parse key");

    match mode.trim() {
        "e" => {
            println!("cipher text:\n{}", encipher(text.as_str(), key));
        }
        "d" => {
            println!("plain text:\n{}", decipher(text.as_str(), key));
        }
        _ => panic!("unknown mode"),
    }
}
